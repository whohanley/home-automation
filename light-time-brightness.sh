#!/usr/bin/env bash

set -e

TUYA_API_URL='https://openapi.tuyaus.com'

source .secrets

DATE_VANCOUVER=$(TZ='America/Vancouver' date '+%F')
SUNSET=$(TZ='America/Vancouver' sunwait list set 48.4259823N -123.3501499E)
SUNSET_TIMESTAMP=$(TZ='America/Vancouver' date --date="${DATE_VANCOUVER} ${SUNSET}" '+%s')
SECONDS_TO_SUNSET=$(echo "${SUNSET_TIMESTAMP} - $(date --utc '+%s')" | bc)

BRIGHTNESS=102 # minimum value as a default. better than blinding myself

# 4 hours after sunset, start dimming
if [ $SECONDS_TO_SUNSET -lt -14400 ] && [ $SECONDS_TO_SUNSET -ge -18000 ]; then
    BRIGHTNESS=210
elif [ $SECONDS_TO_SUNSET -lt -18000 ] && [ $SECONDS_TO_SUNSET -ge -21600 ]; then
    BRIGHTNESS=180
elif [ $SECONDS_TO_SUNSET -lt -21600 ] && [ $SECONDS_TO_SUNSET -ge -25200 ]; then
    BRIGHTNESS=160
elif [ $SECONDS_TO_SUNSET -lt -25200 ] && [ $SECONDS_TO_SUNSET -ge -28800 ]; then
    BRIGHTNESS=102
else
    exit 0 # leave brightness alone outside of these hours
fi

ACCESS_TOKEN=$(./tuya-authenticate.sh)

TIMESTAMP=$(date +%s%N | cut -b1-13)

SIGN=$(echo -n "${TUYA_CLIENT_ID}${ACCESS_TOKEN}${TIMESTAMP}" | openssl dgst -sha256 -hmac "${TUYA_CLIENT_SECRET}" | awk '{ print $NF }' | tr '[:lower:]' '[:upper:]')

DEVICE_STATUS=$(curl --silent --show-error \
     -X GET "${TUYA_API_URL}/v1.0/devices/${BDRM_LIGHT_ID}/status" \
     --header "client_id: ${TUYA_CLIENT_ID}" \
     --header "access_token: ${ACCESS_TOKEN}" \
     --header "t: ${TIMESTAMP}" \
     --header "sign_method: HMAC-SHA256" \
     --header "sign: ${SIGN}" \
     --http1.1)

UPDATED_HSV=$(./reduce-colour-value.py "${BRIGHTNESS}" "${DEVICE_STATUS}")

curl --silent --show-error \
    -X POST "${TUYA_API_URL}/v1.0/devices/${BDRM_LIGHT_ID}/commands" \
    --header "client_id: ${TUYA_CLIENT_ID}" \
    --header "access_token: ${ACCESS_TOKEN}" \
    --header "t: ${TIMESTAMP}" \
    --header "sign_method: HMAC-SHA256" \
    --header "sign: ${SIGN}" \
    --data "{\"commands\": [{\"code\":\"colour_data\", \"value\":${UPDATED_HSV}}]}" \
    --http1.1
