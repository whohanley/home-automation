#!/usr/bin/env bash

set -e

TUYA_API_URL='https://openapi.tuyaus.com'
OPENWEATHER_API_URL='https://api.openweathermap.org/data/2.5'

source .secrets

DEVICE_ID=${1:-$LVRM_LIGHT_ID}

WEATHER=$(curl --silent --show-error "${OPENWEATHER_API_URL}/weather?q=Vancouver,BC,CA&appid=${OPENWEATHER_API_KEY}")
CONDITION=$(echo "${WEATHER}" | jq --raw-output '.weather[0].main')

# Default to cloudy colour
CONDITION_COLOUR='{ "h": 25, "s": 155, "v": 255 }'

case $CONDITION in
    'Clear')
        CONDITION_COLOUR='{ "h": 50, "s": 240, "v": 255 }'
        ;;
    'Clouds')
        CLOUD_COVER_PERCENTAGE=$(echo "${WEATHER}" | jq --raw-output '.clouds.all')
        if [ "${CLOUD_COVER_PERCENTAGE}" -lt 75 ]; then
            # 75% cloud cover is less cloudy than it sounds. any lower, call it clear by Victoria standards
            CONDITION_COLOUR='{ "h": 64, "s": 225, "v": 255 }'
        else
            CONDITION_COLOUR='{ "h": 25, "s": 155, "v": 255 }'
        fi
        ;;
    'Drizzle' | 'Rain' | 'Thunderstorm')
        CONDITION_COLOUR='{ "h": 133, "s": 214, "v": 170 }'
        ;;
    'Snow')
        CONDITION_COLOUR='{ "h": 198, "s": 150, "v": 255 }'
        ;;
    'Smoke' | 'Haze' | 'Dust' | 'Sand' | 'Ash')
        CONDITION_COLOUR='{ "h": 5, "s": 255, "v": 209 }'
        ;;
esac

ACCESS_TOKEN=$(./tuya-authenticate.sh)

TIMESTAMP=$(date +%s%N | cut -b1-13)

SIGN=$(echo -n "${TUYA_CLIENT_ID}${ACCESS_TOKEN}${TIMESTAMP}" | openssl dgst -sha256 -hmac "${TUYA_CLIENT_SECRET}" | awk '{ print $NF }' | tr '[:lower:]' '[:upper:]')

DEVICE_STATUS=$(curl --silent --show-error \
                     -X GET "${TUYA_API_URL}/v1.0/devices/${DEVICE_ID}/status" \
                     --header "client_id: ${TUYA_CLIENT_ID}" \
                     --header "access_token: ${ACCESS_TOKEN}" \
                     --header "t: ${TIMESTAMP}" \
                     --header "sign_method: HMAC-SHA256" \
                     --header "sign: ${SIGN}" \
                     --http1.1)

UPDATED_HSV=$(./update-colour-hue-sat.py "${CONDITION_COLOUR}" "${DEVICE_STATUS}")

curl --silent --show-error \
    -X POST "${TUYA_API_URL}/v1.0/devices/${DEVICE_ID}/commands" \
    --header "client_id: ${TUYA_CLIENT_ID}" \
    --header "access_token: ${ACCESS_TOKEN}" \
    --header "t: ${TIMESTAMP}" \
    --header "sign_method: HMAC-SHA256" \
    --header "sign: ${SIGN}" \
    --data "{ \"commands\": [{ \"code\": \"colour_data\", \"value\": ${UPDATED_HSV} }] }" \
    --http1.1
