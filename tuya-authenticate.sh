#!/usr/bin/env bash

set -e

TUYA_API_URL='https://openapi.tuyaus.com'

source .secrets

TIMESTAMP=$(date +%s%N | cut -b1-13)

SIGN=$(echo -n "${TUYA_CLIENT_ID}${TIMESTAMP}" | openssl dgst -sha256 -hmac "${TUYA_CLIENT_SECRET}" | awk '{ print $NF }' | tr '[:lower:]' '[:upper:]')

AUTH=$(curl --silent --show-error \
            --get ${TUYA_API_URL}/v1.0/token \
            --data grant_type=1 \
            --header "client_id: ${TUYA_CLIENT_ID}" \
            --header "t: ${TIMESTAMP}" \
            --header "sign_method: HMAC-SHA256" \
            --header "sign: ${SIGN}" \
            --http1.1)

ACCESS_TOKEN=$(echo "$AUTH" | jq --raw-output '.result.access_token')
REFRESH_TOKEN=$(echo "$AUTH" | jq --raw-output '.result.refresh_token')

echo "${ACCESS_TOKEN}"
