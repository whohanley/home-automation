#!/usr/bin/env bash

set -e

TUYA_API_URL='https://openapi.tuyaus.com'

source .secrets

sunwait wait set offset 45 48.4259823N -123.3501499E

ACCESS_TOKEN=$(./tuya-authenticate.sh)

TIMESTAMP=$(date +%s%N | cut -b1-13)

SIGN=$(echo -n "${TUYA_CLIENT_ID}${ACCESS_TOKEN}${TIMESTAMP}" | openssl dgst -sha256 -hmac "${TUYA_CLIENT_SECRET}" | awk '{ print $NF }' | tr '[:lower:]' '[:upper:]')

curl --silent --show-error \
    -X POST "${TUYA_API_URL}/v1.0/devices/${BDRM_LIGHT_ID}/commands" \
    --header "client_id: ${TUYA_CLIENT_ID}" \
    --header "access_token: ${ACCESS_TOKEN}" \
    --header "t: ${TIMESTAMP}" \
    --header "sign_method: HMAC-SHA256" \
    --header "sign: ${SIGN}" \
    --data "{\"commands\": [{\"code\":\"switch_led\", \"value\": true}]}" \
    --http1.1
