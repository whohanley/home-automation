#!/usr/bin/env python3

import json
import sys

input_value=int(sys.argv[1])
device_status = json.loads(sys.argv[2])

# device status JSON contains an array called "result" that includes an object
# of the form {code: "colour_value", value: <hsv>} that we want to update
for stat in device_status['result']:
    if stat.get('code') == 'colour_data':
        # not sure why this needs to be decoded again at this point, seems like
        # a flaw in the API
        colour = json.loads(stat['value'])
        colour['v'] = input_value
        print(json.dumps(colour))
