#!/usr/bin/env bash

set -e

TUYA_API_URL='https://openapi.tuyaus.com'

source .secrets

# Uncomment to wait for sunrise before turning on. Currently just turn on at a
# fixed time (via systemd timer)
# sunwait wait rise offset 60 48.4259823N -123.3501499E

ACCESS_TOKEN=$(./tuya-authenticate.sh)

TIMESTAMP=$(date +%s%N | cut -b1-13)

SIGN=$(echo -n "${TUYA_CLIENT_ID}${ACCESS_TOKEN}${TIMESTAMP}" | openssl dgst -sha256 -hmac "${TUYA_CLIENT_SECRET}" | awk '{ print $NF }' | tr '[:lower:]' '[:upper:]')


# Turn on bedroom light. White mode, low brightness

curl --silent --show-error \
     -X POST "${TUYA_API_URL}/v1.0/devices/${BDRM_LIGHT_ID}/commands" \
     --header "client_id: ${TUYA_CLIENT_ID}" \
     --header "access_token: ${ACCESS_TOKEN}" \
     --header "t: ${TIMESTAMP}" \
     --header "sign_method: HMAC-SHA256" \
     --header "sign: ${SIGN}" \
     --data "{\"commands\": [{\"code\":\"work_mode\", \"value\":\"white\"}, {\"code\":\"bright_value\", \"value\":64}, {\"code\":\"switch_led\", \"value\":true}]}" \
     --http1.1

# Ramp up brightness after 10 minutes

sleep 600

TIMESTAMP=$(date +%s%N | cut -b1-13)

SIGN=$(echo -n "${TUYA_CLIENT_ID}${ACCESS_TOKEN}${TIMESTAMP}" | openssl dgst -sha256 -hmac "${TUYA_CLIENT_SECRET}" | awk '{ print $NF }' | tr '[:lower:]' '[:upper:]')

curl --silent --show-error \
     -X POST "${TUYA_API_URL}/v1.0/devices/${BDRM_LIGHT_ID}/commands" \
     --header "client_id: ${TUYA_CLIENT_ID}" \
     --header "access_token: ${ACCESS_TOKEN}" \
     --header "t: ${TIMESTAMP}" \
     --header "sign_method: HMAC-SHA256" \
     --header "sign: ${SIGN}" \
     --data "{\"commands\": [{\"code\":\"bright_value\", \"value\":255}]}" \
     --http1.1

# Change living room light colour according to weather and turn it on at full
# brightness

./light-weather-colour.sh "${LVRM_LIGHT_ID}"
sleep 10

TIMESTAMP=$(date +%s%N | cut -b1-13)

SIGN=$(echo -n "${TUYA_CLIENT_ID}${ACCESS_TOKEN}${TIMESTAMP}" | openssl dgst -sha256 -hmac "${TUYA_CLIENT_SECRET}" | awk '{ print $NF }' | tr '[:lower:]' '[:upper:]')


DEVICE_STATUS=$(curl --silent --show-error \
                     -X GET "${TUYA_API_URL}/v1.0/devices/${LVRM_LIGHT_ID}/status" \
                     --header "client_id: ${TUYA_CLIENT_ID}" \
                     --header "access_token: ${ACCESS_TOKEN}" \
                     --header "t: ${TIMESTAMP}" \
                     --header "sign_method: HMAC-SHA256" \
                     --header "sign: ${SIGN}" \
                     --http1.1)

# Set colour Value to 255 (max brightness)
UPDATED_HSV=$(./update-colour-value.py 255 "${DEVICE_STATUS}")

# Turn on living room light with max brightness
curl --silent --show-error \
     -X POST "${TUYA_API_URL}/v1.0/devices/${LVRM_LIGHT_ID}/commands" \
     --header "client_id: ${TUYA_CLIENT_ID}" \
     --header "access_token: ${ACCESS_TOKEN}" \
     --header "t: ${TIMESTAMP}" \
     --header "sign_method: HMAC-SHA256" \
     --header "sign: ${SIGN}" \
     --data "{\"commands\": [{\"code\":\"colour_data\", \"value\":${UPDATED_HSV}}, {\"code\":\"switch_led\", \"value\":true}]}" \
     --http1.1
